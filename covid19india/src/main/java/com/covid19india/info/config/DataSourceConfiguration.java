package com.covid19india.info.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataSourceConfiguration {
	@Bean(name = "covid19indiaDataSource")

	public DataSource covid19indiaDataSource() {

		HikariConfig config = new HikariConfig("/datasource.properties");
		HikariDataSource dataSource = new HikariDataSource(config);

		return dataSource;
	}

}
