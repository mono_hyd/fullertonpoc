package com.covid19india.info.model;

public class Status {
	private Integer statusCode;
	private String statusMessge;
	public Status(Integer statusCode, String statusMessge) {
		super();
		this.statusCode = statusCode;
		this.statusMessge = statusMessge;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessge() {
		return statusMessge;
	}
	public void setStatusMessge(String statusMessge) {
		this.statusMessge = statusMessge;
	}

}