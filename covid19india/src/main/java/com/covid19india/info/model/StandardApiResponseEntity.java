package com.covid19india.info.model;

public class StandardApiResponseEntity extends ResponseEntity {
	public StandardApiResponseEntity() {
        this.setSuccessStatus();
    }

    public StandardApiResponseEntity(Object data) {
	    this.setSuccessStatus();
	    this.setData(data);
    }
    
}
