package com.covid19india.info.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.covid19india.info.model.Travel_History;

public interface TravelInfoMySqlRepository extends JpaRepository<Travel_History, Integer> {

	@Query(value = "select * from travel_history where timefrom > :timefrom and timeto < :timeto", nativeQuery = true)
	List<Travel_History> findAllByDateNamedParamsNative(@Param("timefrom") String timefrom,
			@Param("timeto") String timeto);

}
