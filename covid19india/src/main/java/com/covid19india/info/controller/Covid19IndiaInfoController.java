package com.covid19india.info.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.covid19india.info.model.StandardApiResponseEntity;
import com.covid19india.info.service.TravelInfoService;

@CrossOrigin(origins = "*")
@RestController
public class Covid19IndiaInfoController {

	@Autowired
	private TravelInfoService travelInfo;

	//On form load it will called to show the data in data table of UI
	@RequestMapping(value = "/gettravelinfo", method = RequestMethod.GET)
	public @ResponseBody StandardApiResponseEntity rejectSBU() {
		StandardApiResponseEntity response = new StandardApiResponseEntity();
		try {
			response = travelInfo.getTravelInfo();

		} catch (Exception e) {

			response.setErrorStatus(e);
		}
		return response;
	}
//get the data from url and save the data to mysql db
	@RequestMapping(value = "/savetomysql", method = RequestMethod.GET)
	public @ResponseBody StandardApiResponseEntity saveToMySql() {
		StandardApiResponseEntity response = new StandardApiResponseEntity();
		try {

			response = travelInfo.saveTravelInfoToMySqlDB();

		} catch (Exception e) {

			response.setErrorStatus(e);
		
		}
		return response;
	}

	//Fetch the data from mysqldb based on given date
	@RequestMapping(value = "/getTravelinfobydate/{fromdate}/{todate}", method = RequestMethod.GET)
	public @ResponseBody StandardApiResponseEntity rejectSBU(@PathVariable(value = "fromdate") String timefrom,
			@PathVariable(value = "todate") String timeto) {
		StandardApiResponseEntity response = new StandardApiResponseEntity();
		try {
			response = travelInfo.getTravelInfoByDate(timefrom, timeto);

		} catch (Exception e) {

			response.setErrorStatus(e);
		}
		return response;
	}
//	//Fetch the data from mysqldb based on given date
//		@RequestMapping(value = "/getTravelinfobydate/{fromdate}/{todate}", method = RequestMethod.GET)
//		public @ResponseBody StandardApiResponseEntity rejectSBU(@PathVariable(value = "fromdate") String timefrom,
//				@PathVariable(value = "todate") String timeto) {
//			StandardApiResponseEntity response = new StandardApiResponseEntity();
//			try {
//				response = travelInfo.getTravelInfoByDate(timefrom, timeto);
//
//			} catch (Exception e) {
//
//				response.setErrorStatus(e);
//			}
//			return response;
//		}
}
