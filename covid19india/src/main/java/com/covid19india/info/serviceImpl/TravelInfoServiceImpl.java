package com.covid19india.info.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.covid19india.info.model.Output;
import com.covid19india.info.model.StandardApiResponseEntity;
import com.covid19india.info.model.Travel_History;
import com.covid19india.info.repository.TravelInfoMySqlRepository;
import com.covid19india.info.service.TravelInfoService;

@Service
public class TravelInfoServiceImpl implements TravelInfoService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	TravelInfoMySqlRepository travelInfoRepository;

	@Value("${travelInfoURL}")
	String travelInfoURL;
	
	@Value("${urltoSaveTravelHistoryToMongodb}")
	String urltoSaveTravelHistoryToMongodb;

	String result="";
	
	@Override
	public StandardApiResponseEntity getTravelInfo() throws Exception {
		travelInfoRepository.findAll();
		return new StandardApiResponseEntity(travelInfoRepository.findAll());
	}

	@Override
	public StandardApiResponseEntity getTravelInfoByDate(String timefrom, String timeto) throws Exception {

		return new StandardApiResponseEntity(travelInfoRepository.findAllByDateNamedParamsNative(timefrom, timeto));
	}

	@Override
	public StandardApiResponseEntity saveTravelInfoToMySqlDB() throws Exception {
		Output travelinfo = restTemplate.getForObject(travelInfoURL, Output.class);
		for (Travel_History data : travelinfo.getTravel_history()) {
			travelInfoRepository.save(data);
		}
		try {
		saveTravelInfoToMongolDB(travelinfo.getTravel_history());
		}catch(Exception e) {
			
		}
		return new StandardApiResponseEntity(travelinfo.getTravel_history());
	}
	//calling a api to save the data to mongodb
	public StandardApiResponseEntity saveTravelInfoToMongolDB(Travel_History[] travelhistory) throws Exception {
		
		result=restTemplate.postForObject(urltoSaveTravelHistoryToMongodb, travelhistory, String.class);
		
		return new StandardApiResponseEntity(result);
	}
	

}
