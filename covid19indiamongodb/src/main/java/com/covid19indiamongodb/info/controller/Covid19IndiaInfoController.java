package com.covid19indiamongodb.info.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.covid19indiamongodb.info.model.StandardApiResponseEntity;
import com.covid19indiamongodb.info.model.Travel_History;
import com.covid19indiamongodb.info.service.TravelInfoService;

@CrossOrigin(origins = "*")
@RestController
public class Covid19IndiaInfoController {
	
	@Autowired
	TravelInfoService travelInfoService;
	//get the data from url and save in to mongodb
	@RequestMapping(value = "/savetomongodb", method = RequestMethod.GET)
	public @ResponseBody StandardApiResponseEntity saveToMongoDB() {
		StandardApiResponseEntity response = new StandardApiResponseEntity();
		try {
			
			response=travelInfoService.saveTravelInfoToMongoDB();
			
		} catch (Exception e) {

			response.setErrorStatus(e);
		}
		return response;
	}

	@RequestMapping(value = "/savetomongodb", method = RequestMethod.POST)
	public @ResponseBody StandardApiResponseEntity dataSaveToMongoDB(@RequestBody Travel_History[] travelhistory) {
		StandardApiResponseEntity response = new StandardApiResponseEntity();
		try {
			
			response=travelInfoService.saveTravelInfoToMongoDB();
			
		} catch (Exception e) {

			response.setErrorStatus(e);
		}
		return response;
	}
	
}
