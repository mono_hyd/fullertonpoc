package com.covid19indiamongodb.info.service;

import com.covid19indiamongodb.info.model.StandardApiResponseEntity;
import com.covid19indiamongodb.info.model.Travel_History;

public interface TravelInfoService {
	
	 public StandardApiResponseEntity saveTravelInfoToMongoDB() throws Exception;
	 public StandardApiResponseEntity saveDataToMongoDB(Travel_History[] travelhistory) throws Exception;
	 

}
