/*
 * package com.covid19indiamongodb.info.config;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.data.mongodb.MongoDbFactory;
 * 
 * import com.mongodb.MongoClientOptions; import com.mongodb.ServerAddress;
 * import com.mongodb.client.MongoClient;
 * 
 * public class MongoConfig {
 * 
 * @Bean
 * 
 * @Autowired public MongoDbFactory mongoDbFactory(MongoSettingsProperties
 * properties) {
 * 
 * // Client configuration (number of connections, copy cluster verification)
 * MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
 * builder.connectionsPerHost(properties.getMaxConnectionsPerHost());
 * builder.minConnectionsPerHost(properties.getMinConnectionsPerHost()); if
 * (properties.getReplicaSet() != null) {
 * builder.requiredReplicaSetName(properties.getReplicaSet()); }
 * builder.threadsAllowedToBlockForConnectionMultiplier(
 * properties.getThreadsAllowedToBlockForConnectionMultiplier());
 * builder.serverSelectionTimeout(properties.getServerSelectionTimeout());
 * builder.maxWaitTime(properties.getMaxWaitTime());
 * builder.maxConnectionIdleTime(properties.getMaxConnectionIdleTime());
 * builder.maxConnectionLifeTime(properties.getMaxConnectionLifeTime());
 * builder.connectTimeout(properties.getConnectTimeout());
 * builder.socketTimeout(properties.getSocketTimeout()); //
 * builder.socketKeepAlive(properties.getSocketKeepAlive());
 * builder.sslEnabled(properties.getSslEnabled());
 * builder.sslInvalidHostNameAllowed(properties.getSslInvalidHostNameAllowed());
 * builder.alwaysUseMBeans(properties.getAlwaysUseMBeans());
 * builder.heartbeatFrequency(properties.getHeartbeatFrequency());
 * builder.minHeartbeatFrequency(properties.getMinHeartbeatFrequency());
 * builder.heartbeatConnectTimeout(properties.getHeartbeatConnectTimeout());
 * builder.heartbeatSocketTimeout(properties.getHeartbeatSocketTimeout());
 * builder.localThreshold(properties.getLocalThreshold()); Mongo
 * mongoClientOptions = builder.build();
 * 
 * // MongoDB address list List<ServerAddress> serverAddresses = new
 * ArrayList<ServerAddress>(); for (String address : properties.getAddress()) {
 * String[] hostAndPort = address.split(":"); String host = hostAndPort[0];
 * Integer port = Integer.parseInt(hostAndPort[1]); ServerAddress serverAddress
 * = new ServerAddress(host, port); serverAddresses.add(serverAddress); }
 * 
 * //logger.info("serverAddresses:" + serverAddresses.toString());
 * 
 * // connection authentication // MongoCredential mongoCredential = null; // if
 * (properties.getUsername() != null) { // mongoCredential =
 * MongoCredential.createScramSha1Credential( // properties.getUsername(),
 * properties.getAuthenticationDatabase() != null // ?
 * properties.getAuthenticationDatabase() : properties.getDatabase(), //
 * properties.getPassword().toCharArray()); // }
 * 
 * // Create an authentication client // MongoClient mongoClient = new
 * MongoClient(serverAddresses, mongoCredential, mongoClientOptions);
 * 
 * // Create a non-authenticated client MongoClient mongoClient = new
 * MongoClient(serverAddresses, mongoClientOptions);
 * 
 * // Create MongoDbFactory MongoDbFactory mongoDbFactory = new
 * SimpleMongoDbFactory(mongoClient, properties.getDatabase()); return
 * mongoDbFactory; }
 * 
 * 
 * }
 */