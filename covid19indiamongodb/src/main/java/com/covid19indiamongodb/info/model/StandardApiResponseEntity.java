package com.covid19indiamongodb.info.model;

public class StandardApiResponseEntity extends ResponseEntity {
	public StandardApiResponseEntity() {
        this.setSuccessStatus();
    }

    public StandardApiResponseEntity(Object data) {
	    this.setSuccessStatus();
	    this.setData(data);
    }
    
}
