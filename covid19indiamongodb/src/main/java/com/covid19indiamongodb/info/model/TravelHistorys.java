package com.covid19indiamongodb.info.model;

import java.util.Arrays;

public class TravelHistorys {
	private Travel_History[] travel_history;

	public Travel_History[] getTravelinfo() {
		return travel_history;
	}

	public void setTravelinfo(Travel_History[] travel_history) {
		this.travel_history = travel_history;
	}

	@Override
	public String toString() {
		return "TravelHistory [travel_history=" + Arrays.toString(travel_history) + "]";
	}

	
	

}
