package com.covid19indiamongodb.info.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.covid19indiamongodb.info.model.Travel_History;

@Repository
public interface TravelInfoMongoRepository extends MongoRepository<Travel_History, Integer> {

}
